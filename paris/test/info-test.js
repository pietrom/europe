const assert = require('assert')
const {describe, it} = require("mocha")
const Hello = require('../hello')
const hello = new Hello('Ciao')
describe('Hello', function () {
    describe('when getMessage is invoked', function () {
        it('should return message with default target', function () {
            assert.equal(hello.getMessage(), 'Ciao, World!')
        })

        it('should return message with provided target', function () {
            assert.equal(hello.getMessage('pietrom'), 'Ciao, pietrom!')
        })
    })
})
