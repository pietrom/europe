module.exports = function Hello(base) {
	this.getMessage = function(to) {
		return `${base}, ${to || 'World'}!`
	}
}