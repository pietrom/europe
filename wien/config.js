const { v4: uuidv4 } = require('uuid');

function getEnv(key, fallback) {
	return process.env[key] || fallback
}

function readMeasures() {
	const measures = [{
		name: getEnv("DEVICE_MEASURE_0_NAME", "value"),
		average: parseInt(getEnv("DEVICE_MEASURE_0_AVERAGE", 15)),
		range: parseInt(getEnv("DEVICE_MEASURE_0_RANGE", 20))
	}]
	let i = 1
	let name
	while(name = getEnv(`DEVICE_MEASURE_${i}_NAME`, "")) {
		measures.push({
			name: name,
			average: parseInt(getEnv(`DEVICE_MEASURE_${i}_AVERAGE`, 15)),
			range: parseInt(getEnv(`DEVICE_MEASURE_${i}_RANGE`, 20))
		})
		i++
	}
	
	return measures
}

module.exports = {
	rabbitmq: {
		url: getEnv("RABBITMQ_URL", "amqp://guest:guest@localhost:5674/europe"),
		exchange: getEnv("RABBITMQ_EXCHANGE", "measures"),
		exchangeType: getEnv("RABBITMQ_EXCHANGE_TYPE", "direct"),
		queue: getEnv("RABBITMQ_QUEUE", "test_queue"),
		routingKey: getEnv("RABBITMQ_ROUTING_KEY", "")
	},    
    deviceId: getEnv("DEVICE_ID", uuidv4()),
    deviceInterval: parseInt(getEnv("DEVICE_INTERVAL", 2000)),
    measures: readMeasures()
}