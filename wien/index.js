const amqplib = require('amqplib');

function random(average, range) {
    return average - range / 2 + Math.floor(Math.random() * range)
}

const config = require('./config')
console.log('Starting', config)

const { rabbitmq, deviceId, deviceInterval, measures } = config
const { url, exchange, exchangeType, queue, routingKey } = rabbitmq

async function run() {
    var conn = await amqplib.connect(url, "heartbeat=60");
    var ch = await conn.createChannel()
    
    await ch.assertExchange(exchange, exchangeType, {durable: true}).catch(console.error);
    await ch.assertQueue(queue, {durable: true});
    await ch.bindQueue(queue, exchange, routingKey);
    const job = setInterval(async function() {
        const msg = {
            deviceId: deviceId,
        }
        for(let measure of config.measures) {
            msg[measure.name] = random(measure.average, measure.range)
        }
        const msgText = JSON.stringify(msg)
        console.log(deviceId, 'publishing', msgText)
    	await ch.publish(exchange, routingKey, Buffer.from(msgText));
    }, deviceInterval);

    return function shutdown() {
        console.info('Shutting down', deviceId)
        clearInterval(job);
        ch.close();
        conn.close();
    }  
}

run().then(shutdown => {
    process.on('SIGINT', function() {
        shutdown()
        process.exit()
    });
    process.on('SIGTERM', function() {
        shutdown()
        process.exit()
    });
})
