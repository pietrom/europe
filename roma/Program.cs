﻿using System.Text;
using System.Text.Json;
using Leek;
using Leek.DependencyInversion;
using Leek.Inbox;
using Leek.Logging;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace Roma;

public class Program {
    private static string GetEnv(string key, string fallbackValue) {
        return Environment.GetEnvironmentVariable(key) ?? fallbackValue;
    }

    private static RabbitMqMessageBusConfiguration ReadRabbitMqConfig() {
        return new RabbitMqMessageBusConfiguration(GetEnv("RABBITMQ_HOST", "localhost"),
            int.Parse(GetEnv("RABBITMQ_PORT", "5674")), GetEnv("RABBITMQ_VIRTUAL_HOST", "europe"),
            GetEnv("RABBITMQ_USERNAME", "guest"), GetEnv("RABBITMQ_PASSWORD", "guest"));
    }

    public async static Task Main(string[] args) {
        IHost host = Host.CreateDefaultBuilder(args).ConfigureServices(services => {
            var mqConfig = ReadRabbitMqConfig();
            Console.WriteLine($"RabbitMq config is {mqConfig}");
            services.AddMessageBus(mqConfig)
                .AddDefaultMessageSerializer().AddDefaultStringEncoder()
                .AddConsoleLoggerFactory()
                .AddMessageConsumer<JsonDocument, StringMessageLogger>(new RabbitMqConsumerConfiguration(
                    new QueueConfiguration("logger"), new ExchangeConfiguration("measures")))
                .AddSingleton<IHostedService, LoggerService>();
        }).Build();
        await host.RunAsync();
    }

    class LoggerService : IHostedService {
        private readonly RabbitMqMessageBus bus;

        public LoggerService(RabbitMqMessageBus bus) {
            this.bus = bus;
        }

        public Task StartAsync(CancellationToken cancellationToken) {
            bus.Start();
            return Task.CompletedTask;
        }

        public Task StopAsync(CancellationToken cancellationToken) {
            bus.Stop();
            return Task.CompletedTask;
        }
    }
}

public class StringMessageLogger : MessageHandler<JsonDocument> {
    private readonly Logger logger;

    public StringMessageLogger(LoggerFactory loggerFactory) {
        logger = loggerFactory.GetLogger<StringMessageLogger>();
    }

    public Task Handle(JsonDocument msg) {
        logger.Info($"Received '{msg.ToJsonString()}'");
        return Task.CompletedTask;
    }
}

public static class JsonDocumentExtension {
    public static string ToJsonString(this JsonDocument jdoc) {
        using (var stream = new MemoryStream())
        {
            Utf8JsonWriter writer = new Utf8JsonWriter(stream, new JsonWriterOptions { Indented = true });
            jdoc.WriteTo(writer);
            writer.Flush();
            return Encoding.UTF8.GetString(stream.ToArray());
        }
    }
}
